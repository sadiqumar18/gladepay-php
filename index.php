<?php
require 'vendor/autoload.php';
require 'src/GladePay/GladePay.php';


$gp = new GladePay\GladePay("GP0000001", "123456789", "https://demo.api.gladepay.com");

$json_initiate = '
{
    "action":"initiate",
    "paymentType":"card",
    "user": {
        "firstname":"Abubakar",
        "lastname":"Ango",
        "email":"sadiq@payit.ng",
        "ip":"192.168.33.10",
        "fingerprint": "cccvxbxbxb"
    },
    "card":{
        "card_no":"5438898014560229",
        "expiry_month":"09",
        "expiry_year":"19",
        "ccv":"789",
        "pin":"3310"
    },
    "amount":"10000",
    "country": "NG",
    "currency": "NGN"
}
';


$json_request = json_decode($json_initiate, true);


//echo $json_file;
//print_r($json_request); exit;

// Normal Card payment
//echo $gp->cardPayment($json_request['user'], $json_request['card'], $json_request['amount'], $json_request['country'], $json_request['currency']);

// Recurrent Card Payment
//echo $gp->cardPayment($json_request['user'], $json_request['card'], $json_request['amount'], $json_request['country'], $json_request['currency'], 'recurrent', $json_request['recurrent']);


// Installmental Card Payment
//echo $gp->cardPayment($json_request['user'], $json_request['card'], $json_request['amount'], $json_request['country'], $json_request['currency'], 'installment', $json_request['installment']);



//echo $gp->validateOTP("GP270019221Y","12345");

echo $gp->verifyTransaction("GP270019221Y");